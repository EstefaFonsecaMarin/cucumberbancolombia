package com.accenture.Bancolombia.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/Features",
        tags= {"@Feature1","@tag1"},
        glue = {"com.accenture.Bancolombia.step_definitions"},
        snippets = SnippetType.UNDERSCORE,
        monochrome = true,
        dryRun = false,
        plugin = {"json:reportJson/cucumber.json"}
)

public class Runner {
}







