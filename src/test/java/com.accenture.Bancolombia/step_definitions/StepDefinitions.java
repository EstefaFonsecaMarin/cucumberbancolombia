package com.accenture.Bancolombia.step_definitions;
import com.accenture.Bancolombia.steps.StepsBancolombia;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepDefinitions {


    @Steps
    StepsBancolombia Ban;



    @Given("^that Bancolombia page is open$")
    public void that_bancolombia_page_is_open() throws Throwable {
    Ban.openBrowser();
    Ban.prepareBaseLine(Ban.getDriver());


    }

    @When("^the user clicks transactions$")
    public void the_user_clicks_transactions() throws Throwable {
        Ban.clickTransaction();
        Ban.screenshots(Ban.getDriver());
        Ban.compareImages();

    }

    @Then("^the screenshots should be compared in a report$")
    public void the_screenshots_should_be_compared_in_a_report() throws Throwable {
      //  Ban.screenshots();


    }

    @And("^the screenshots are taken$")
    public void the_screenshots_are_taken() throws Exception {

    }


}
