package com.accenture.Bancolombia.steps;

import com.accenture.Bancolombia.ScreenCaptureUtility;
import com.accenture.Bancolombia.pages.BancolombiaPage;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;


public class StepsBancolombia extends PageObject {


    BancolombiaPage homePage;
    BancolombiaPage click;
    BancolombiaPage screenshots;
    WebDriver driver;
    ScreenCaptureUtility screen = new ScreenCaptureUtility();

   public void openBrowser() {

       /*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
       driver = new ChromeDriver();
       driver.get("https://www.grupobancolombia.com/wps/portal/personas");*/

homePage.open();

   }

    public void clickTransaction() {
        /*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.grupobancolombia.com/wps/portal/personas");*/
        click.ClickOptionTransaction();

    }

    public void screenshots (WebDriver driver){

        new ScreenCaptureUtility().takePageScreenshot(driver,"homePage");


        }
    public void compareImages()
    {
        new ScreenCaptureUtility().areImageEquals("homePage", "homePage" );

        getDriver().quit();

    }


    public void prepareBaseLine(WebDriver driver) {
        new ScreenCaptureUtility().prepareBaseline(driver,"homePage");

    }
}
