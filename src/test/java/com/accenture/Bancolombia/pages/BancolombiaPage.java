package com.accenture.Bancolombia.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;


@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class BancolombiaPage extends PageObject {

    @FindBy(xpath = "//li[@id='transacciones']//a[contains(text(),'Transacciones')]") private WebElement Transactions;


public void ClickOptionTransaction(){

    Transactions.click();
}


}




