@Feature1
Feature: FUNCTIONALITY - open the Bancolombia home page and click on the transactions option
HOW opening Bancolombia
WANT click on the transactions option
TO  apply the knowledge obtained with cucumber.

Background: Open Bancolombia page
Given that Bancolombia page is open


@tag1
Scenario: PROOF TO BE PERFORMED-Take Screenshots, compared them  and make a report
When the user clicks transactions
And  the screenshots are taken
Then the screenshots should be compared in a report